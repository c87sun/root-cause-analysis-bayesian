# Root Cause Analysis Bayesian

This repo holds the workshop contents in root cause analysis using Bayesian Networks and a tutorial of using PyAgrum package for quick deployment of the Bayesian Analysis. 



## Getting started

0. create a conda or pip environment on local machine, or using google collab.

1. Install the PyAgrum package by


if conda:
```
conda install -c conda-forge pyagrum
```

if pip:
```
pip install pyagrum
```

2. Activate the environment and run jupyter-lab and go through the note book contents.



## Models and Data

The sample models and data are stored in the subfolders in /models and /data





## Authors

Chen Sun

May 2023